<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('department_id')->nullable()->constrained();
            $table->foreignId('file_type_id')->nullable()->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('financial_year_id')->nullable()->constrained();
            $table->string('file_no');
            $table->boolean('is_dmc')->default(0);
            $table->foreignId('dmc_user_id')->nullable()->constrained('users');
            $table->enum('dmc_status', ['0', '1', '2'])->nullable()->comment('0 => pending, 1 => Rejected, 2 => Accepted');
            $table->date('dmc_status_date')->nullable();
            $table->text('dmc_remark')->nullable();
            $table->string('subject')->nullable();
            $table->text('remark')->nullable();
            $table->date('date');
            $table->boolean('is_close')->default(0);
            $table->datetime('close_date')->nullable();
            $table->text('close_remark')->nullable();
            $table->foreignId('to_id')->nullable()->constrained('users');
            $table->boolean('is_forward')->default(0);
            $table->boolean('is_file_forward')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
