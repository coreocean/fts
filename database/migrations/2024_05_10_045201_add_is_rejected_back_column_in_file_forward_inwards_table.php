<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('file_forward_inwards', function (Blueprint $table) {
            $table->boolean('is_rejected_back')->default(0)->after('is_accepted');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('file_forward_inwards', function (Blueprint $table) {
            $table->dropColumn('is_rejected_back');
        });
    }
};
