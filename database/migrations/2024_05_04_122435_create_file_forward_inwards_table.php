<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('file_forward_inwards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('file_id')->nullable()->constrained();
            $table->foreignId('from_department_id')->nullable()->constrained('departments');
            $table->foreignId('from_id')->nullable()->constrained('users');
            $table->foreignId('to_department_id')->nullable()->constrained('departments');
            $table->foreignId('to_id')->nullable()->constrained('users');
            $table->foreignId('method_id')->nullable()->constrained();
            $table->enum('is_accepted', ['0', '1', '2'])->default(0)->comment('0 => pending, 1 => Rejected, 2 => Accepted');
            $table->text('remark')->nullable();
            $table->string('person_name')->nullable();
            $table->datetime('forward_date')->nullable();
            $table->datetime('inward_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('file_forward_inwards');
    }
};
