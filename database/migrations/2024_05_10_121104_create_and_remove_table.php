<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('file_types', function (Blueprint $table) {
            $table->dropColumn('sequence');
        });

        Schema::table('departments', function (Blueprint $table) {
            $table->string('initial')->after('name');
            $table->integer('sequence')->after('initial');
        });

        Schema::dropIfExists('file_departments');

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('file_types', function (Blueprint $table) {
            $table->integer('sequence')->default(1);
        });

        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('initial');
            $table->dropColumn('sequence');
        });

        Schema::create('file_departments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('file_id')->nullable()->constrained();
            $table->foreignId('department_id')->nullable()->constrained();
            $table->timestamps();
        });
    }
};
