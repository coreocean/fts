<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            [
                'id' => 1,
                'name' => 'dashboard.view',
                'group' => 'dashboard',
            ],
            [
                'id' => 2,
                'name' => 'users.view',
                'group' => 'users',
            ],
            [
                'id' => 3,
                'name' => 'users.create',
                'group' => 'users',
            ],
            [
                'id' => 4,
                'name' => 'users.edit',
                'group' => 'users',
            ],
            [
                'id' => 5,
                'name' => 'users.delete',
                'group' => 'users',
            ],
            [
                'id' => 6,
                'name' => 'users.toggle_status',
                'group' => 'users',
            ],
            [
                'id' => 7,
                'name' => 'users.change_password',
                'group' => 'users',
            ],
            [
                'id' => 8,
                'name' => 'roles.view',
                'group' => 'roles',
            ],
            [
                'id' => 9,
                'name' => 'roles.create',
                'group' => 'roles',
            ],
            [
                'id' => 10,
                'name' => 'roles.edit',
                'group' => 'roles',
            ],
            [
                'id' => 11,
                'name' => 'roles.delete',
                'group' => 'roles',
            ],
            [
                'id' => 12,
                'name' => 'roles.assign',
                'group' => 'roles',
            ],
            [
                'id' => 13,
                'name' => 'department.view',
                'group' => 'department',
            ],
            [
                'id' => 14,
                'name' => 'department.create',
                'group' => 'department',
            ],
            [
                'id' => 15,
                'name' => 'department.edit',
                'group' => 'department',
            ],
            [
                'id' => 16,
                'name' => 'financial-year.view',
                'group' => 'financial year',
            ],
            [
                'id' => 17,
                'name' => 'financial-year.create',
                'group' => 'financial year',
            ],
            [
                'id' => 18,
                'name' => 'financial-year.edit',
                'group' => 'financial year',
            ],
            [
                'id' => 19,
                'name' => 'file-type.view',
                'group' => 'file type',
            ],
            [
                'id' => 20,
                'name' => 'file-type.create',
                'group' => 'file type',
            ],
            [
                'id' => 21,
                'name' => 'file-type.edit',
                'group' => 'file type',
            ],
            [
                'id' => 22,
                'name' => 'method.view',
                'group' => 'method',
            ],
            [
                'id' => 23,
                'name' => 'method.create',
                'group' => 'method',
            ],
            [
                'id' => 24,
                'name' => 'method.edit',
                'group' => 'method',
            ],
            [
                'id' => 25,
                'name' => 'file.view',
                'group' => 'file',
            ],
            [
                'id' => 26,
                'name' => 'file.add',
                'group' => 'file',
            ],
            [
                'id' => 27,
                'name' => 'file.forward',
                'group' => 'file',
            ],
            [
                'id' => 28,
                'name' => 'file.inward',
                'group' => 'file',
            ],
            [
                'id' => 29,
                'name' => 'file.close',
                'group' => 'file',
            ],
            [
                'id' => 30,
                'name' => 'file.verify',
                'group' => 'file',
            ],
            [
                'id' => 31,
                'name' => 'file.verified',
                'group' => 'file',
            ],
            [
                'id' => 32,
                'name' => 'file.rejected',
                'group' => 'file',
            ],
            [
                'id' => 33,
                'name' => 'file.transit',
                'group' => 'file',
            ],
            [
                'id' => 34,
                'name' => 'file.department-file',
                'group' => 'file',
            ],
            [
                'id' => 35,
                'name' => 'report.department',
                'group' => 'report',
            ],
            [
                'id' => 36,
                'name' => 'report.employees',
                'group' => 'report',
            ],
            [
                'id' => 37,
                'name' => 'report.file-status',
                'group' => 'report'
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::updateOrCreate([
                'id' => $permission['id']
            ], [
                'id' => $permission['id'],
                'name' => $permission['name'],
                'group' => $permission['group']
            ]);
        }
    }
}
