<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class DefaultLoginUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // Super Admin Seeder ##
        $superAdminRole = Role::updateOrCreate(['name' => 'Super Admin']);
        $permissions = Permission::pluck('id', 'id')->all();
        $superAdminRole->syncPermissions($permissions);

        $user = User::updateOrCreate([
            'email' => 'superadmin@gmail.com'
        ], [
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'mobile' => '9999999991',
            'password' => Hash::make('12345678'),
        ]);
        $user->assignRole([$superAdminRole->id]);

        // User Seeder ##
        $users = Role::updateOrCreate(['name' => 'User']);
        // $permissions = Permission::pluck('id', 'id')->all();
        // $users->syncPermissions($permissions);

        // Department HOD Seeder ##
        $departmentHod = Role::updateOrCreate(['name' => 'Department HOD']);
        // $permissions = Permission::pluck('id', 'id')->all();
        // $departmentHod->syncPermissions($permissions);

        // Administrative Seeder ##
        $administrativeRole = Role::updateOrCreate(['name' => 'Administrative AMC']);
        // $permissions = Permission::pluck('id', 'id')->all();
        // $administrativeRole->syncPermissions($permissions);

        // Administrative Seeder ##
        $administrativeRole = Role::updateOrCreate(['name' => 'Administrative DMC']);
        // $permissions = Permission::pluck('id', 'id')->all();
        // $administrativeRole->syncPermissions($permissions);
    }
}
