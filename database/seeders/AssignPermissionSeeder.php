<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AssignPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdmin = Role::where('name', 'Super Admin')->first();
        $superAdmin->syncPermissions(['dashboard.view', 'file.view', 'file.add', 'file.forward', 'file.inward', 'file.close', 'users.view', 'users.create', 'users.edit', 'users.delete', 'users.toggle_status', 'users.change_password', 'roles.view', 'roles.create', 'roles.edit', 'roles.delete', 'roles.assign', 'department.view', 'department.create', 'department.edit', 'financial-year.view', 'financial-year.create', 'financial-year.edit', 'file-type.view', 'file-type.create', 'file-type.edit', 'method.view', 'method.create', 'method.edit', 'file.transit', 'report.department', 'report.employees', 'report.file-status']);


        $user = Role::where('name', 'User')->first();
        $user->syncPermissions(['dashboard.view', 'file.view', 'file.add', 'file.forward', 'file.inward', 'file.close', 'file.transit', 'report.department', 'report.employees', 'report.file-status']);


        $departmentHod = Role::where('name', 'Department HOD')->first();
        $departmentHod->syncPermissions(['dashboard.view', 'file.view', 'file.add', 'file.forward', 'file.inward', 'file.close', 'file.transit', 'file.department-file', 'report.department', 'report.employees', 'report.file-status']);


        $administrativeAMC = Role::where('name', 'Administrative AMC')->first();
        $administrativeAMC->syncPermissions(['dashboard.view', 'file.view', 'file.forward', 'file.inward', 'file.close', 'file.transit', 'file.department-file', 'report.department', 'report.employees', 'report.file-status']);


        $administrativeDMC = Role::where('name', 'Administrative DMC')->first();
        $administrativeDMC->syncPermissions(['dashboard.view', 'file.view', 'file.forward', 'file.inward', 'file.close', 'file.verify', 'file.verified', 'file.rejected', 'file.transit', 'file.department-file', 'report.department', 'report.employees', 'report.file-status']);
    }
}
