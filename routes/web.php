<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Master\DepartmentController;
use App\Http\Controllers\Master\FileTypeController;
use App\Http\Controllers\Master\FinancialYearController;
use App\Http\Controllers\Master\MethodController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\DMCFilePermissionController;
use App\Http\Controllers\DepartmentFileController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
})->name('/');




// Guest Users
Route::middleware(['guest', 'PreventBackHistory', 'firewall.all'])->group(function () {
    Route::get('login', [App\Http\Controllers\Admin\AuthController::class, 'showLogin'])->name('login');
    Route::post('login', [App\Http\Controllers\Admin\AuthController::class, 'login'])->name('signin');
    Route::get('register', [App\Http\Controllers\Admin\AuthController::class, 'showRegister'])->name('register');
    Route::post('register', [App\Http\Controllers\Admin\AuthController::class, 'register'])->name('signup');
});




// Authenticated users
Route::middleware(['auth', 'PreventBackHistory', 'firewall.all'])->group(function () {

    // Auth Routes
    Route::get('home', fn () => redirect()->route('dashboard'))->name('home');
    Route::get('dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
    Route::post('logout', [App\Http\Controllers\Admin\AuthController::class, 'Logout'])->name('logout');
    Route::get('change-theme-mode', [App\Http\Controllers\Admin\DashboardController::class, 'changeThemeMode'])->name('change-theme-mode');
    Route::get('show-change-password', [App\Http\Controllers\Admin\AuthController::class, 'showChangePassword'])->name('show-change-password');
    Route::post('change-password', [App\Http\Controllers\Admin\AuthController::class, 'changePassword'])->name('change-password');




    // Users Roles n Permissions
    Route::resource('users', App\Http\Controllers\Admin\UserController::class);
    Route::get('users/{user}/toggle', [App\Http\Controllers\Admin\UserController::class, 'toggle'])->name('users.toggle');
    Route::get('users/{user}/retire', [App\Http\Controllers\Admin\UserController::class, 'retire'])->name('users.retire');
    Route::put('users/{user}/change-password', [App\Http\Controllers\Admin\UserController::class, 'changePassword'])->name('users.change-password');
    Route::get('users/{user}/get-role', [App\Http\Controllers\Admin\UserController::class, 'getRole'])->name('users.get-role');
    Route::put('users/{user}/assign-role', [App\Http\Controllers\Admin\UserController::class, 'assignRole'])->name('users.assign-role');
    Route::resource('roles', App\Http\Controllers\Admin\RoleController::class);

    // Route for master
    Route::name('master.')->prefix('master/')->group(function () {
        Route::resource('department', DepartmentController::class);
        Route::resource('file-type', FileTypeController::class);
        Route::resource('financial-year', FinancialYearController::class);
        Route::resource('method', MethodController::class);
    });

    // file Route for forward
    Route::get('file/forward/get-department', [FileController::class, 'getForwardDepartment'])->name('file.forward-department');
    Route::get('file/forward/get-department-user', [FileController::class, 'getForwardDepartmentUser'])->name('file.forward-department-user');
    Route::get('file/forward/get-administrative-user', [FileController::class, 'getForwardAdministrativeUser'])->name('file.forward-administrative-user');
    Route::get('file/forward', [FileController::class, 'forward'])->name('file.forward');
    Route::get('file/show-forward-details', [FileController::class, 'showForwardDetails'])->name('files.showForwardDetails');
    Route::post('file/forward/store', [FileController::class, 'storeForward'])->name('file.storeForward');

    // file route for inward
    Route::get('file/inward', [FileController::class, 'inward'])->name('file.inward');
    Route::get('file/show-inward-details', [FileController::class, 'showInwardDetails'])->name('files.showInwardDetails');
    Route::post('file/inward/store', [FileController::class, 'storeInward'])->name('file.storeInward');

    // dmc file route
    Route::get('file/dmc/verify', [DMCFilePermissionController::class, 'index'])->name('dmcfile.index');
    Route::post('file/dmc-file/store', [DMCFilePermissionController::class, 'store'])->name('dmcfile.store');
    Route::get('file/dmc/verified', [DMCFilePermissionController::class, 'verified'])->name('dmcfile.verified');
    Route::get('file/dmc/rejected', [DMCFilePermissionController::class, 'rejected'])->name('dmcfile.rejected');

    // route for close file
    Route::get('file/close', [FileController::class, 'close'])->name('file.close');
    Route::post('file/close/store', [FileController::class, 'saveClose'])->name('file.saveClose');

    // file route for create
    Route::get('file/show-details', [FileController::class, 'showDetails'])->name('files.showDetails');
    Route::get('file/transit', [FileController::class, 'transit'])->name('files.transit');
    Route::get('file/qrcode', [FileController::class, 'qrCode'])->name('files.qrCode');
    Route::resource('file', FileController::class);

    // route for department file
    Route::get('department-files', [DepartmentFileController::class, 'getUserDepartmentFiles'])->name('department.getUserDepartmentFiles');

    // route for report
    Route::get('report/department', [ReportController::class, 'department'])->name('report.department');
    Route::get('report/employee', [ReportController::class, 'employee'])->name('report.employee');
    Route::get('report/file-status', [ReportController::class, 'fileStatus'])->name('report.fileStatus');
});




Route::get('/php', function (Request $request) {
    if (!auth()->check())
        return 'Unauthorized request';

    Artisan::call($request->artisan);
    return dd(Artisan::output());
});
