<x-admin.layout>
    <x-slot name="title">Department Report</x-slot>
    <x-slot name="heading">Department Report</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Department Name</th>
                                    <th>Created</th>
                                    <th>DMC Rejected</th>
                                    <th>In transit / Pending</th>
                                    <th>Closed</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $department)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $department->name }}</td>
                                    <td>{{ $department->created_count }}</td>
                                    <td>{{ $department->dmc_rejected }}</td>
                                    <td>{{ $department->pending_count }}</td>
                                    <td>{{ $department->closed_count }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>



