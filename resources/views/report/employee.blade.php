<x-admin.layout>
    <x-slot name="title">Employee Wise Report</x-slot>
    <x-slot name="heading">Employee Wise Report</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="my-2 mb-5">
                        <form method="get">
                            <div class="mb-3 row">
                                <div class="col-md-3">
                                    <label class="col-form-label" for="from">From Date</label>
                                    <input class="form-control" id="from" name="from" type="date" value="@if(isset(Request()->from)){{ Request()->from }}@endif">
                                </div>
                                <div class="col-md-3">
                                    <label class="col-form-label" for="to">To Date</label>
                                    <input class="form-control" id="to" name="to" type="date" value="@if(isset(Request()->to)){{ Request()->to }}@endif">
                                </div>
                                <div class="col-md-3">
                                    <label class="col-form-label" for="user">Select User</label>
                                    <select name="user" id="user_id" class="form-select js-example-basic-multiple">
                                        <option value="">Select</option>
                                        @foreach($employees as $employee)
                                        <option @if(isset(Request()->user) && Request()->user == $employee->id)selected @endif value="{{ $employee->id }}">{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-form-label" for="to">&nbsp;</div>
                                    <button class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Department Name</th>
                                    <th>User Name</th>
                                    <th>File No.</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Closed On</th>
                                    <th>Pending Days</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $report?->department->name }}</td>
                                    <td>{{ $report?->user->name }}</td>
                                    <td>{{ $report->file_no }}</td>
                                    <td>{{ $report->subject }}</td>
                                    <td>
                                        @if($report?->is_file_forward == "0")
                                        Created
                                        @elseif($report->is_close == "1" && $report->is_file_forward == "1")
                                        Closed
                                        @elseif($report?->is_file_forward == "1" && $report->is_close == "0")
                                        In transit
                                        @endif
                                    </td>
                                    <td>{{ date('d-m-Y', strtotime($report->date)) }}</td>
                                    <td>{{ ($report->close_date) ? date('d-m-Y', strtotime($report->close_date)) : '-' }}</td>
                                    <td>
                                        @php
                                        $closeDate = ($report->close_date) ? date('Y-m-d', strtotime($report->close_date)) : date('Y-m-d');
                                        $diff = strtotime($report->date) - strtotime($closeDate);

                                        $daysleft = abs(round($diff / 86400));
                                        @endphp

                                        {{ $daysleft }}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>



