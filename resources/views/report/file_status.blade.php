<x-admin.layout>
    <x-slot name="title">File Status Report</x-slot>
    <x-slot name="heading">File Status Report</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Department Name</th>
                                    <th>Total Created</th>
                                    <th>In transit / Pending</th>
                                    @foreach($users as $user)
                                    <th>{{ $user->name }}</th>
                                    @endforeach

                                    @foreach($departments as $department)
                                    <th>{{ $department->name }}</th>
                                    @endforeach

                                    <th>Closed</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($fileStatus as $file)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $file['name'] }}</td>
                                    <td>{{ $file['totalCreated'] }}</td>
                                    <td>{{ $file['pending'] }}</td>
                                    @foreach($file[0] as $user)
                                    <td>{{ $user }}</td>
                                    @endforeach

                                    @foreach($departments as $department)
                                        <td>
                                        @if($department->id == $file['departmentId'])
                                        {{ $file['remaining'] }}
                                        @else
                                        0
                                        @endif
                                        </td>
                                    @endforeach

                                    <td>{{ $file['close'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>



