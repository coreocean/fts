<x-admin.layout>
    <x-slot name="title">Department File List</x-slot>
    <x-slot name="heading">Department File List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>DMC Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            @if($file->dmc_status == "0")
                                            <span class="badge bg-warning">Pending</span>
                                            @elseif($file->dmc_status == "1")
                                            <span class="badge bg-danger">Rejected</span>
                                            @elseif($file->dmc_status == "2")
                                            <span class="badge bg-success">Accepted</span>
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button>
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('file.details')

</x-admin.layout>

