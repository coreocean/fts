<x-admin.layout>
    <x-slot name="title">Forward File List</x-slot>
    <x-slot name="heading">Forward File List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                <tr>
                                    <td>{{ $loop->iteration }}111</td>
                                    <td>{{ $file->file_no }}</td>
                                    <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                    <td>{{ $file?->fileType?->name }}</td>
                                    <td>{{ $file->subject }}</td>
                                    <td>
                                        <button class="view-element btn btn-success btn-sm" data-id="{{ $file->id }}" data-is-file-forward="{{ $file->is_file_forward }}" data-bs-toggle="modal" data-bs-target=".forwardModel">Forward</button>
                                        <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- model for forward file --}}
    <div class="modal fade forwardModel" role="dialog" aria-labelledby="forwardModelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form id="addForm" action="{{ route('file.storeForward') }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="forwardModelLabel">Created File Details</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body" id="stockData">

                            <div class="table-responsive">
                                <table class="table table-bordered border-dark">
                                    <thead>
                                        <tr>
                                            <th>Department</th>
                                            <th>Created By</th>
                                            <th>File No.</th>
                                            <th>Date</th>
                                            <th>File Type</th>
                                        </tr>
                                    </thead>
                                    <tbody id="forwardFileTbodyModel">

                                    </tbody>
                                </table>
                            </div>

                            <h4 class="mt-3">Inward Forward File Details</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered border-dark">
                                    <thead>
                                        <tr align="center">
                                            <th colspan="2">Forward By</th>
                                            <th colspan="2">Forward to</th>
                                            <th rowspan="2">Status</th>
                                            <th rowspan="2">Datetime</th>
                                            <th rowspan="2">Method</th>
                                            <th rowspan="2">Remark</th>
                                        </tr>
                                        <tr align="center">
                                            <th>Department</th>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody id="forwardFileDetailsTbodyModel"></tbody>
                                </table>
                            </div>

                            <div class="row mt-3">
                                <input type="hidden" name="from_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" id="file_id" name="file_id" value="">
                                <input type="hidden" id="file_inward_forward_id" name="file_inward_forward_id" value="">
                                <input type="hidden" id="is_file_forward" name="is_file_forward" value="">
                                <div class="col-4 mb-3">
                                    <label for="form-label">Select type <span class="text-danger">*</span></label>
                                    <select name="type_id" id="selectRoleType" class="form-select" required>
                                        <option value="">Select type</option>
                                        @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-4 mb-3 d-none departmentHodDepartmentDiv">
                                    <label for="form-label">Select department <span class="text-danger">*</span></label>
                                    <select name="department_id" id="selectDepartmentHodDepartment" class="form-select">

                                    </select>
                                </div>

                                <div class="col-4 mb-3 d-none departmentUserDiv">
                                    <label for="form-label">Select user <span class="text-danger">*</span></label>
                                    <select name="department_user_id" id="selectDepartmentHodDepartmentUser" class="form-select">

                                    </select>
                                </div>

                                <div class="col-4 mb-3 d-none administrativeUserDiv">
                                    <label for="form-label">Select user <span class="text-danger">*</span></label>
                                    <select name="administrative_user_id" id="selectAdministrativeUser" class="form-select">

                                    </select>
                                </div>


                                <div class="col-4 mb-3">
                                    <label for="form-label">Select method <span class="text-danger">*</span></label>
                                    <select name="method_id" class="form-select" required>
                                        <option value="">Select method</option>
                                        @foreach($methods as $method)
                                            <option value="{{ $method->id }}">{{ $method->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-4 mb-3">
                                    <label for="form-label">Enter file sending person name</label>
                                    <input type="text" class="form-control" name="person_name" placeholder="Enter method person name" />
                                </div>

                                <div class="col-4 mb-3">
                                    <label for="form-label">Remark</label>
                                    <textarea name="remark" placeholder="Enter remark" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveForwardSubmit" class="btn btn-primary">Forward Send</button>
                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- end of model for forward file --}}




    @include('file.details')


</x-admin.layout>


{{-- forward model ajax --}}
<script>
    $('body').on('click', '.view-element', function(){
        var model_id = $(this).attr("data-id");
        var isFileForward = $(this).attr('data-is-file-forward');
        document.getElementById('file_id').value = model_id;
        document.getElementById('file_inward_forward_id').value = isFileForward;
        var url = "{{ route('files.showForwardDetails') }}";

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                id: model_id,
            },
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data, textStatus, jqXHR) {
                if (!data.error) {
                    // code for file details
                    let fileHtml = `<tr>
                            <td>${data.file.department.name}</td>
                            <td>${data.file.user.name}</td>
                            <td>${data.file.file_no}</td>
                            <td>${data.file.date}</td>
                            <td>${data.file.file_type.name}</td>
                        </tr>
                        <tr>
                        <th>Subject</th>
                        <td colspan="4">${data.file.subject}</td>
                        </tr>`;
                    document.getElementById('forwardFileTbodyModel').innerHTML = fileHtml;
                    // end of code for file details


                    let html = "";
                    if(data.file.file_forward_inward && data.file.file_forward_inward.from_id){
                            document.getElementById('file_inward_forward_id').value = data.file.file_forward_inward.id;

                            let status = "";

                            if(data.file.file_forward_inward.is_accepted == "0"){
                                status = `<span class="badge bg-warning text-dark">Pending</span>`;
                            }else if(data.file.file_forward_inward.is_accepted == "1"){
                                status = `<span class="badge bg-danger text-dark">Rejected</span>`;
                            }else if(data.file.file_forward_inward.is_accepted == "2"){
                                status = `<span class="badge bg-success text-dark">Accepted</span>`;
                            }
                            html += `<tr align="center">
                                <td>${(data.file.file_forward_inward.from_department) ? data.file.file_forward_inward.from_department.name : data.file.file_forward_inward.from.roles[0].name}</td>
                                <td>${data.file.file_forward_inward.from.name}</td>
                                <td>${(data.file.file_forward_inward.to_department) ? data.file.file_forward_inward.to_department.name : data.file.file_forward_inward.to.roles[0].name}</td>
                                <td>${data.file.file_forward_inward.to.name}</td>
                                <td>${status}</td>
                                <td>${data.file.file_forward_inward.forward_date}</td>
                                <td>${data.file.file_forward_inward.method.name} ${(data.file.file_forward_inward.person_name) ? '('+data.file.file_forward_inward.person_name+')' : '-'}</td>
                                <td>${(data.file.file_forward_inward.remark) ? data.file.file_forward_inward.remark : '-'}</td>
                            </tr>`;

                    }else{
                        html += `<tr align="center">
                                <td colspan="7">No Data Found</td>
                            </tr>`;
                    }
                    $('#forwardFileDetailsTbodyModel').html(html);
                } else {
                    swal("Error!", data.error, "error");
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });
    })
</script>
{{-- end of forward model ajax --}}

{{-- save forward model --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#saveForwardSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('file.storeForward') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data)
            {
                $("#saveForwardSubmit").prop('disabled', false);
                if (!data.error)
                    swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('file.forward') }}';
                        });
                else
                    swal("Error!", data.error, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#saveForwardSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#saveForwardSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });

    });
</script>
{{-- end of save forward model --}}


{{-- select role type script --}}
<script>
    $(document).ready(function(){
        $('#selectRoleType').change(function(){
            let role = $(this).find(':selected').text();
            let role_id = $(this).val();

            if(role_id == ""){
                $('.administrativeUserDiv').addClass('d-none');
                $('.departmentHodDepartmentDiv').addClass('d-none');
                $('.departmentUserDiv').addClass('d-none');
            }else{

                if(role == "Department HOD" || role == "User"){
                    $('.administrativeUserDiv').addClass('d-none');
                    $('.departmentHodDepartmentDiv').removeClass('d-none');
                    $('.departmentUserDiv').addClass('d-none');
                    $('#selectDepartmentHodDepartmentUser').html('');
                    $('#selectAdministrativeUser').html('');
                    $('#selectDepartmentHodDepartmentUser').prop('required', false);
                    $('#selectAdministrativeUser').prop('required', false);

                    $.ajax({
                        url: '{{ route('file.forward-department') }}',
                        type: 'GET',
                        data: {
                            'role_id': role_id
                        },
                        beforeSend: function()
                        {
                            $('#preloader').css('opacity', '0.5');
                            $('#preloader').css('visibility', 'visible');
                        },
                        success: function(data)
                        {
                            let html = "<option value=''>Select department</option>";
                            $.each(data.departments, function(key, val){
                                html += `<option value="${val.id}">${val.name}</option>`;
                            })
                            $('#selectDepartmentHodDepartment').html(html);
                            $('#selectDepartmentHodDepartment').prop('required', true);
                        },
                        complete: function() {
                            $('#preloader').css('opacity', '0');
                            $('#preloader').css('visibility', 'hidden');
                        },
                    });
                }else{
                    $('.administrativeUserDiv').removeClass('d-none');
                    $('.departmentHodDepartmentDiv').addClass('d-none');
                    $('.departmentUserDiv').addClass('d-none');
                    $('#selectDepartmentHodDepartmentUser').html('');
                    $('#selectDepartmentHodDepartment').html('');
                    $('#selectDepartmentHodDepartmentUser').prop('required', false);
                    $('#selectDepartmentHodDepartment').prop('required', false);

                    $.ajax({
                        url: '{{ route('file.forward-administrative-user') }}',
                        type: 'GET',
                        data: {
                            'role_id': role_id
                        },
                        beforeSend: function()
                        {
                            $('#preloader').css('opacity', '0.5');
                            $('#preloader').css('visibility', 'visible');
                        },
                        success: function(data)
                        {
                            let html = "<option value=''>Select user</option>";
                            $.each(data.users, function(key, val){
                                html += `<option value="${val.id}">${val.name}</option>`;
                            })
                            $('#selectAdministrativeUser').html(html);
                            $('#selectAdministrativeUser').prop('required', true);
                        },
                        complete: function() {
                            $('#preloader').css('opacity', '0');
                            $('#preloader').css('visibility', 'hidden');
                        },
                    });
                }
            }

        });


        // select department
        $('body').on('change', '#selectDepartmentHodDepartment', function(){
            let departmentId = $(this).val();
            let roleId = $('#selectRoleType').val();

            $('.administrativeUserDiv').addClass('d-none');
            $('.departmentHodDepartmentDiv').removeClass('d-none');
            $('.departmentUserDiv').removeClass('d-none');
            $('#selectAdministrativeUser').html('');
            $('#selectAdministrativeUser').prop('required', false);


            $.ajax({
                url: '{{ route('file.forward-department-user') }}',
                type: 'GET',
                data: {
                    'department_id': departmentId,
                    'role_id': roleId
                },
                beforeSend: function()
                {
                    $('#preloader').css('opacity', '0.5');
                    $('#preloader').css('visibility', 'visible');
                },
                success: function(data)
                {
                    let html = "<option value=''>Select User</option>";
                    $.each(data.users, function(key, val){
                        html += `<option value="${val.id}">${val.name}</option>`;
                    })
                    $('#selectDepartmentHodDepartmentUser').html(html);
                    $('#selectDepartmentHodDepartmentUser').prop('required', true);
                },
                complete: function() {
                    $('#preloader').css('opacity', '0');
                    $('#preloader').css('visibility', 'hidden');
                },
            });
        });
    });
</script>
{{-- end of select role type script --}}
