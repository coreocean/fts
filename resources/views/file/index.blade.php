<x-admin.layout>
    <x-slot name="title">Create File List</x-slot>
    <x-slot name="heading">Create File List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}

    @can('file.add')
    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-header">
                        <h4 class="card-title">Add File</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="date">Date <span class="text-danger">*</span></label>
                                <input class="form-control" id="date" value="{{ date('Y-m-d') }}" name="date" type="date" max="{{ date('Y-m-d') }}" required />
                                <span class="text-danger is-invalid date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label for="fileTypeId" class="col-form-label">Select file type <span class="text-danger">*</span></label>
                                <select name="file_type_id" id="fileTypeId" class="form-select" required>
                                    <option value="">Select file type</option>
                                    @foreach($fileTypes as $fileType)
                                    <option value="{{ $fileType->id }}">{{ $fileType->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="subject">Subject <span class="text-danger">*</span></label>
                                <input class="form-control" id="subject" name="subject" type="text" placeholder="Enter subject" required />
                                <span class="text-danger is-invalid subject_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="remark">Remark</label>
                                <textarea class="form-control" id="remark" name="remark" placeholder="Enter remark" ></textarea>
                                <span class="text-danger is-invalid remark_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="user_id">Created By <span class="text-danger">*</span></label>
                                <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                <input class="form-control" id="user_id" type="text" value="{{ $user->name }}" style="background:#cdcbcb7d" readonly />
                                <span class="text-danger is-invalid user_id_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="department_id">Select File Department <span class="text-danger">*</span></label>
                                <select name="department_id" class="form-select" required>
                                    @if(count($user?->userDepartments) >1)
                                    <option value="">Select Department</option>
                                    @endif
                                    @foreach($user?->userDepartments as $department)
                                    <option value="{{ $department?->department->id }}">{{ $department?->department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger is-invalid department_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="financial_year_id">Financial Year <span class="text-danger">*</span></label>
                                <input type="hidden" name="financial_year_id" value="{{ ($financialYear) ? $financialYear->id : '' }}" />
                                <input class="form-control" id="financial_year_id" name="year" type="text" value="{{ ($financialYear) ? $financialYear->year : '' }}" style="background:#cdcbcb7d" readonly />
                                <span class="text-danger is-invalid financial_year_id_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endcan


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('file.add')
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>DMC Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            @if($file->dmc_status == "0")
                                            <span class="badge bg-warning text-dark">Pending</span>
                                            @elseif($file->dmc_status == "1")
                                            <span class="badge bg-danger text-dark">Rejected</span>
                                            @elseif($file->dmc_status == "2")
                                            <span class="badge bg-success text-dark">Accepted</span>
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".generateQRCodeModel" class="view-qrcode btn btn-success btn-sm">Generate Qrcode</button>

                                            <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- model for forward file --}}
    <div class="modal fade generateQRCodeModel" tabindex="-1" role="dialog" aria-labelledby="generateQRCodeModelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="generateQRCodeModelLabel">File QRCode</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card-body" id="qrCodeData">


                    </div>

                    <button class="btn btn-primary" onclick="printDiv('qrCodeData')">Print Qrcode</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- end of model for forward file --}}





@include('file.details')



</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        let date = $('#date').val();
        if("{{ date('Y-m-d') }}" != date){
            swal({
                title: "You selected previous date please approve your file from Administrative Department",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) =>
            {
                if (justTransfer)
                {
                    $("#addSubmit").prop('disabled', true);

                    var formdata = new FormData(this);
                    $.ajax({
                        url: '{{ route('file.store') }}',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        beforeSend: function()
                        {
                            $('#preloader').css('opacity', '0.5');
                            $('#preloader').css('visibility', 'visible');
                        },
                        success: function(data)
                        {
                            $("#addSubmit").prop('disabled', false);
                            if (!data.error)
                                swal("Successful!", data.success, "success")
                                    .then((action) => {
                                        window.location.href = '{{ route('file.index') }}';
                                    });
                            else
                                swal("Error!", data.error, "error");
                        },
                        statusCode: {
                            422: function(responseObject, textStatus, jqXHR) {
                                $("#addSubmit").prop('disabled', false);
                                resetErrors();
                                printErrMsg(responseObject.responseJSON.errors);
                            },
                            500: function(responseObject, textStatus, errorThrown) {
                                $("#addSubmit").prop('disabled', false);
                                swal("Error occured!", "Something went wrong please try again", "error");
                            }
                        },
                        complete: function() {
                            $('#preloader').css('opacity', '0');
                            $('#preloader').css('visibility', 'hidden');
                        },
                    });
                }
            })
        }else{
            $("#addSubmit").prop('disabled', true);

            var formdata = new FormData(this);
            $.ajax({
                url: '{{ route('file.store') }}',
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                beforeSend: function()
                {
                    $('#preloader').css('opacity', '0.5');
                    $('#preloader').css('visibility', 'visible');
                },
                success: function(data)
                {
                    $("#addSubmit").prop('disabled', false);
                    if (!data.error)
                        swal("Successful!", data.success, "success")
                            .then((action) => {
                                window.location.href = '{{ route('file.index') }}';
                            });
                    else
                        swal("Error!", data.error, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#addSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#addSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                },
                complete: function() {
                    $('#preloader').css('opacity', '0');
                    $('#preloader').css('visibility', 'hidden');
                },
            });
        }




    });
</script>



{{-- forward model ajax --}}
<script>
    $('body').on('click', '.view-qrcode', function(){
        var model_id = $(this).attr("data-id");
        var url = "{{ route('files.qrCode') }}";

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                id: model_id
            },
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data, textStatus, jqXHR) {
                console.log(data)
                if (!data.error) {
                    $('#qrCodeData').html(data.data);
                } else {
                    swal("Error!", data.error, "error");
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });
    });


    function printDiv(divId) {
        // var content = document.getElementById(divId).innerHTML;
        let styleCss = "{{ asset('/admin/datatables/responsive/2.2.9/css/responsive.bootstrap.min.css') }}"
        var content = $('body').find('#'+divId).html();
        console.log(content)
        var printWindow = window.open();
        printWindow.document.open();
        printWindow.document.write('<html><head><title>Print</title><link rel="stylesheet" href="' + styleCss + '"></head><body>' + content + '</body></html>');
        printWindow.document.close();
        printWindow.print();
        printWindow.close();
    }
</script>
{{-- end of forward model ajax --}}

