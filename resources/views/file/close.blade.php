<x-admin.layout>
    <x-slot name="title">Close File List</x-slot>
    <x-slot name="heading">Close File List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Remark</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            @if($file->is_close == "0")
                                            <span class="badge bg-warning text-dark">In transit</span>
                                            @else
                                            <span class="badge bg-success text-dark">Closed</span>
                                            @endif
                                        </td>
                                        <td>{{ ($file->dmc_remark) ? $file->dmc_remark : '-' }}</td>
                                        <td>
                                            @if($file->is_close == "0")
                                            <button class=" btn btn-primary btn-sm view-element" data-id="{{ $file->id }}" data-bs-toggle="modal" data-bs-target=".inwardModel">Close</button>
                                            @endif

                                            <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- model for inward file --}}
    <div class="modal fade inwardModel" tabindex="-1" role="dialog" aria-labelledby="inwardModelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form id="addForm" action="{{ route('file.saveClose') }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="inwardModelLabel">Close File</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body" id="stockData">

                            <div class="row mt-3">
                                <input type="hidden" id="file_id" name="file_id" value="">

                                <div class="col-12 mb-3">
                                    <label for="form-label">Remark</label>
                                    <textarea name="remark" placeholder="Enter remark" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveInwardSubmit" class="btn btn-primary">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- end of model for inward file --}}



    @include('file.details')



</x-admin.layout>



{{-- save inward model --}}
<script>
    $('.view-element').click(function(){
        let id = $(this).attr('data-id');
        $('#file_id').val(id);
    });

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#saveInwardSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: "{{ route('file.saveClose') }}",
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data)
            {
                $("#saveInwardSubmit").prop('disabled', false);
                if (!data.error)
                    swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('file.close') }}';
                        });
                else
                    swal("Error!", data.error, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#saveInwardSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#saveInwardSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });

    });
</script>
{{-- end of save inward model --}}
