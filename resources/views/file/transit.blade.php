<x-admin.layout>
    <x-slot name="title">File Transit List</x-slot>
    <x-slot name="heading">File Transit List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Last Forwarded to</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            @if($file?->fileForwardInward)
                                                @if($file->fileForwardInward?->is_accepted == 1)
                                                Rejected
                                                @elseif($file->fileForwardInward?->is_accepted == 2)
                                                Accepted
                                                @else
                                                Pending
                                                @endif
                                            @else
                                            Created
                                            @endif
                                        </td>
                                        <td>{{ $file?->fileForwardInward?->to->name }}</td>
                                        <td><button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('file.details')
</x-admin.layout>


