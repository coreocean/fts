<x-admin.layout>
    <x-slot name="title">Inward File List</x-slot>
    <x-slot name="heading">Inward File List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            <button class=" btn btn-success btn-sm view-element" data-id="{{ $file->id }}" data-bs-toggle="modal" data-bs-target=".inwardModel">Inward</button>
                                            <button data-id="{{ $file->id }}"  data-bs-toggle="modal" data-bs-target=".inwardForwardModel" class="view-element btn btn-primary btn-sm">View</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- model for inward file --}}
    <div class="modal fade inwardModel" tabindex="-1" role="dialog" aria-labelledby="inwardModelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form id="addForm" action="{{ route('file.storeInward') }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="inwardModelLabel">Created File Details</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body" id="stockData">

                            <div class="table-responsive">
                                <table class="table table-bordered border-dark">
                                    <thead>
                                        <tr>
                                            <th>Department</th>
                                            <th>Created By</th>
                                            <th>File No.</th>
                                            <th>Date</th>
                                            <th>File Type</th>
                                        </tr>
                                    </thead>
                                    <tbody id="inwardFileTbodyModel">

                                    </tbody>
                                </table>
                            </div>

                            <h4 class="mt-3">Inward Forward File Details</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered border-dark">
                                    <thead>
                                        <tr align="center">
                                            <th colspan="2">Forward By</th>
                                            <th colspan="2">Forward to</th>
                                            <th rowspan="2">Status</th>
                                            <th rowspan="2">Datetime</th>
                                            <th rowspan="2">Method</th>
                                            <th rowspan="2">Remark</th>
                                        </tr>
                                        <tr align="center">
                                            <th>Department</th>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody id="inwardFileDetailsTbodyModel"></tbody>
                                </table>
                            </div>

                            <div class="row mt-3">
                                <input type="hidden" id="fileForwardInwardId" name="file_forward_inward_id" value="">
                                <input type="hidden" id="file_id" name="file_id" value="">


                                <div class="col-4 mb-3">
                                    <label for="form-label">Select Status <span class="text-danger">*</span></label>
                                    <select name="is_accepted" class="form-select disableRejectStatus" required>
                                        <option value="">Select status</option>
                                        <option value="2">Accepted</option>
                                        <option value="1">Rejected</option>
                                    </select>
                                </div>


                                <div class="col-4 mb-3">
                                    <label for="form-label">Remark</label>
                                    <textarea name="remark" placeholder="Enter remark" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="saveInwardSubmit" class="btn btn-primary">Inward Send</button>
                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- end of model for inward file --}}




    @include('file.details')


</x-admin.layout>


{{-- inward model ajax --}}
<script>
    $('body').on('click', '.view-element', function(){
        var model_id = $(this).attr("data-id");
        document.getElementById('file_id').value = model_id;
        var url = "{{ route('files.showInwardDetails') }}";

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                id: model_id
            },
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data, textStatus, jqXHR) {
                console.log(data)
                if (!data.error) {
                    // code for file details
                    let departmentName = "";
                    $.each(data.file.file_departments, function(key, val){
                        departmentName += val.department.name+', ';
                    })

                    let fileHtml = `<tr>
                            <td>${departmentName}</td>
                            <td>${data.file.user.name}</td>
                            <td>${data.file.file_no}</td>
                            <td>${data.file.date}</td>
                            <td>${data.file.file_type.name}</td>
                        </tr>
                        <tr>
                        <th>Subject</th>
                        <td colspan="4">${data.file.subject}</td>
                        </tr>`;
                    document.getElementById('inwardFileTbodyModel').innerHTML = fileHtml;
                    // end of code for file details


                    let html = "";
                    if(data.file.file_forward_inward && data.file.file_forward_inward.from_id){
                            document.getElementById('fileForwardInwardId').value = data.file.file_forward_inward.id;

                            let status = "";

                            if(data.file.file_forward_inward.is_accepted == "0"){
                                status = `<span class="badge bg-warning text-dark">Pending</span>`;
                            }else if(data.file.file_forward_inward.is_accepted == "1"){
                                status = `<span class="badge bg-danger text-dark">Rejected</span>`;
                            }else if(data.file.file_forward_inward.is_accepted == "2"){
                                status = `<span class="badge bg-success text-dark">Accepted</span>`;
                            }

                            html += `<tr align="center">
                                <td>${(data.file.file_forward_inward.from_department) ? data.file.file_forward_inward.from_department.name : data.file.file_forward_inward.from.roles[0].name}</td>
                                <td>${data.file.file_forward_inward.from.name}</td>
                                <td>${(data.file.file_forward_inward.to_department) ? data.file.file_forward_inward.to_department.name : data.file.file_forward_inward.to.roles[0].name}</td>
                                <td>${data.file.file_forward_inward.to.name}</td>
                                <td>${status}</td>
                                <td>${data.file.file_forward_inward.forward_date}</td>
                                <td>${data.file.file_forward_inward.method.name} ${(data.file.file_forward_inward.person_name) ? '('+data.file.file_forward_inward.person_name+')' : '-'}</td>
                                <td>${(data.file.file_forward_inward.remark) ? data.file.file_forward_inward.remark : '-'}</td>
                            </tr>`;

                            if(data.file.file_forward_inward.is_rejected_back){
                                $('body').find('.disableRejectStatus').find('option[value="1"]').prop('disabled', true)
                            }

                    }else{
                        html += `<tr align="center">
                                <td colspan="6">No Data Found</td>
                            </tr>`;
                    }
                    $('#inwardFileDetailsTbodyModel').html(html);
                } else {
                    swal("Error!", data.error, "error");
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });
    })
</script>
{{-- end of inward model ajax --}}

{{-- save inward model --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#saveInwardSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('file.storeInward') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data)
            {
                $("#saveInwardSubmit").prop('disabled', false);
                if (!data.error)
                    swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('file.inward') }}';
                        });
                else
                    swal("Error!", data.error, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#saveInwardSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#saveInwardSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });

    });
</script>
{{-- end of save inward model --}}
