 {{-- model for forward file --}}
<div class="modal fade inwardForwardModel" tabindex="-1" role="dialog" aria-labelledby="inwardForwardModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inwardForwardModelLabel">Created File Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body" id="stockData">

                    <div class="table-responsive">
                        <table class="table table-bordered border-dark">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Created By</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                </tr>
                            </thead>
                            <tbody id="viewForwardFileTbodyModel">

                            </tbody>
                        </table>
                    </div>

                    <h4 class="mt-3">File Records</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered border-dark">
                            <thead>
                                <tr align="center">
                                    <th colspan="2">Forward By</th>
                                    <th colspan="2">Forward to</th>
                                    <th rowspan="2">Status</th>
                                    <th rowspan="2">Datetime</th>
                                    <th rowspan="2">Method</th>
                                    <th rowspan="2">Remark</th>
                                </tr>
                                <tr align="center">
                                    <th>Department / Type</th>
                                    <th>Name</th>
                                    <th>Department / Type</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody id="viewForwardFileDetailsTbodyModel"></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{-- end of model for forward file --}}


@push('scripts')
{{-- forward model ajax --}}
<script>
    $('body').on('click', '.view-element', function(){
        var model_id = $(this).attr("data-id");
        var url = "{{ route('files.showDetails') }}";

        $.ajax({
            url: url,
            type: 'GET',
            data: {
                id: model_id
            },
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data, textStatus, jqXHR) {
                if (!data.error) {
                    // code for file details

                    let fileHtml = `<tr>
                            <td>${data.file.department.name}</td>
                            <td>${data.file.user.name}</td>
                            <td>${data.file.file_no}</td>
                            <td>${data.file.date}</td>
                            <td>${data.file.file_type.name}</td>
                        </tr>
                        <tr>
                            <th>Subject</th>
                            <td colspan="4">${data.file.subject}</td>
                        </tr>`;
                    document.getElementById('viewForwardFileTbodyModel').innerHTML = fileHtml;
                    // end of code for file details


                    let html = "";
                    if(data.file.file_forward_inwards && data.file.file_forward_inwards.length > 0){
                        let htmls = data.file.file_forward_inwards.map(function(val){
                            let status = "";

                            if(val.is_accepted == "0"){
                                status = `<span class="badge bg-warning text-dark">Pending</span>`;
                            }else if(val.is_accepted == "1"){
                                status = `<span class="badge bg-danger text-dark">Rejected</span>`;
                            }else if(val.is_accepted == "2"){
                                status = `<span class="badge bg-success text-dark">Accepted</span>`;
                            }
                            html += `<tr>
                                <td>${(val.from_department) ? val.from_department.name : val.from.roles[0].name}</td>
                                <td>${val.from.name}</td>
                                <td>${(val.to_department) ? val.to_department.name : val.to.roles[0].name}</td>
                                <td>${val.to.name}</td>
                                <td>${status}</td>
                                <td>${val.forward_date}</td>
                                <td>${val.method.name}</td>
                                <td>${(val.remark) ? val.remark : '-'}</td>
                            </tr>`;
                        })
                        html += htmls;

                    }else{
                        html += `<tr align="center">
                                <td colspan="7">No Data Found</td>
                            </tr>`;
                    }
                    $('#viewForwardFileDetailsTbodyModel').html(html);
                } else {
                    swal("Error!", data.error, "error");
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });
    });
</script>
{{-- end of forward model ajax --}}
@endpush
