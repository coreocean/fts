<x-admin.layout>
    <x-slot name="title">Financial Year</x-slot>
    <x-slot name="heading">Financial Year</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}

        @can('financial-year.create')
        <!-- Add Form -->
        <div class="row" id="addContainer" style="display:none;">
            <div class="col-sm-12">
                <div class="card">
                    <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                        @csrf

                        <div class="card-header">
                            <h4 class="card-title">Add Financial Year</h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-3 row">
                                <div class="col-md-4">
                                    <label class="col-form-label" for="year">Year <span class="text-danger">*</span></label>
                                    <input class="form-control" id="year" name="year" type="text" placeholder="Enter year">
                                    <span class="text-danger is-invalid year_err"></span>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-form-label" for="from_date">From Date <span class="text-danger">*</span></label>
                                    <input class="form-control" id="from_date" name="from_date" type="date">
                                    <span class="text-danger is-invalid from_date_err"></span>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label" for="to_date">To Date <span class="text-danger">*</span></label>
                                    <input class="form-control" id="to_date" name="to_date" type="date">
                                    <span class="text-danger is-invalid to_date_err"></span>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label" for="status1">Status <span class="text-danger">*</span></label>
                                    <select name="status" class="form-select" id="status1">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <span class="text-danger is-invalid status_err"></span>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endcan


        @can('financial-year.edit')
        {{-- Edit Form --}}
        <div class="row" id="editContainer" style="display:none;">
            <div class="col">
                <form class="form-horizontal form-bordered" method="post" id="editForm">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Financial Year</h4>
                        </div>
                        <div class="card-body py-2">
                            <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                            <div class="mb-3 row">
                                <div class="col-md-4">
                                    <label class="col-form-label" for="year">Year <span class="text-danger">*</span></label>
                                    <input class="form-control" id="year" name="year" type="text" placeholder="Enter year">
                                    <span class="text-danger is-invalid year_err"></span>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-form-label" for="from_date">From Date <span class="text-danger">*</span></label>
                                    <input class="form-control" id="from_date" name="from_date" type="date">
                                    <span class="text-danger is-invalid from_date_err"></span>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label" for="to_date">To Date <span class="text-danger">*</span></label>
                                    <input class="form-control" id="to_date" name="to_date" type="date">
                                    <span class="text-danger is-invalid to_date_err"></span>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label" for="status1">Status <span class="text-danger">*</span></label>
                                    <select name="status" class="form-select" id="status1">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <span class="text-danger is-invalid status_err"></span>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" id="editSubmit">Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endcan


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @can('financial-year.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endcan
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Year</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Status</th>
                                        @can('financial-year.edit')<th>Action</th>@endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($financialYears as $financialYear)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $financialYear->year }}</td>
                                            <td>{{ date('d-m-Y', strtotime($financialYear->from_date)) }}</td>
                                            <td>{{ date('d-m-Y', strtotime($financialYear->to_date)) }}</td>
                                            <td>
                                                @if($financialYear->status)
                                                <span class="badge bg-success">Active</span>
                                                @else
                                                <span class="badge bg-danger">Inactive</span>
                                                @endif
                                            </td>
                                            @can('financial-year.edit')
                                            <td>
                                                <button class="edit-element btn text-secondary px-2 py-1" title="Edit financial year" data-id="{{ $financialYear->id }}"><i data-feather="edit"></i></button>
                                            </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('master.financial-year.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data)
            {
                $("#addSubmit").prop('disabled', false);
                if (!data.error)
                    swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('master.financial-year.index') }}';
                        });
                else
                    swal("Error!", data.error, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('master.financial-year.edit', ":model_id") }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            beforeSend: function()
            {
                $('#preloader').css('opacity', '0.5');
                $('#preloader').css('visibility', 'visible');
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error)
                {
                    $("#editForm input[name='edit_model_id']").val(data.financialYear.id);
                    $("#editForm input[name='year']").val(data.financialYear.year);
                    $("#editForm input[name='from_date']").val(data.financialYear.from_date);
                    $("#editForm input[name='to_date']").val(data.financialYear.to_date);
                    $("#editForm select[name='status']").val(data.financialYear.status);
                }
                else
                {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
            complete: function() {
                $('#preloader').css('opacity', '0');
                $('#preloader').css('visibility', 'hidden');
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('master.financial-year.update', ":model_id") }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                beforeSend: function()
                {
                    $('#preloader').css('opacity', '0.5');
                    $('#preloader').css('visibility', 'visible');
                },
                success: function(data)
                {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error)
                        swal("Successful!", data.success, "success")
                            .then((action) => {
                                window.location.href = '{{ route('master.financial-year.index') }}';
                            });
                    else
                        swal("Error!", data.error, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                },
                complete: function() {
                    $('#preloader').css('opacity', '0');
                    $('#preloader').css('visibility', 'hidden');
                },
            });

        });
    });
</script>
