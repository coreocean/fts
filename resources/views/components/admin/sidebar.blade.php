<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo-sm.png') }}" alt="" height="22" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo-dark.png') }}" alt="" height="17" />
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo-sm.png') }}" alt="" height="22" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo-light.png') }}" alt="" height="17" />
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu"></div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title">
                    <span data-key="t-menu">Menu</span>
                </li>

                @can('dashboard.view')
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}" >
                        <i class="ri-dashboard-2-line"></i>
                        <span data-key="t-dashboards">Dashboard</span>
                    </a>
                </li>
                @endcan

                @canany(['department.view', 'financial-year.view', 'file-type.view', 'method.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('master.department.*') ? 'active' : '' }} {{ request()->routeIs('master.financial-year.*') ? 'active' : '' }} {{ request()->routeIs('master.file-type.*') ? 'active' : '' }} {{ request()->routeIs('master.method.*') ? 'active' : '' }}" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Masters</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('department.view')
                            <li class="nav-item">
                                <a href="{{ route('master.department.index') }}" class="nav-link {{ request()->routeIs('master.department.*') ? 'active' : '' }}" data-key="t-horizontal">Department</a>
                            </li>
                            @endcan
                            @can('financial-year.view')
                            <li class="nav-item">
                                <a href="{{ route('master.financial-year.index') }}" class="nav-link {{ request()->routeIs('master.financial-year.*') ? 'active' : '' }}" data-key="t-horizontal">Financial Year</a>
                            </li>
                            @endcan
                            @can('file-type.view')
                            <li class="nav-item">
                                <a href="{{ route('master.file-type.index') }}" class="nav-link {{ request()->routeIs('master.file-type.*') ? 'active' : '' }}" data-key="t-horizontal">File Type</a>
                            </li>
                            @endcan
                            @can('method.view')
                            <li class="nav-item">
                                <a href="{{ route('master.method.index') }}" class="nav-link {{ request()->routeIs('master.method.*') ? 'active' : '' }}" data-key="t-horizontal">Method</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan


                @canany(['users.view', 'roles.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('users.*') ? 'active' : '' }} {{ request()->routeIs('roles.*') ? 'active' : '' }}" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="bx bx-user-circle"></i>
                        <span data-key="t-layouts">User Management</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('users.view')
                                <li class="nav-item">
                                    <a href="{{ route('users.index') }}" class="nav-link {{ request()->routeIs('users.*') ? 'active' : '' }}" data-key="t-horizontal">Users</a>
                                </li>
                            @endcan
                            @can('roles.view')
                                <li class="nav-item">
                                    <a href="{{ route('roles.index') }}" class="nav-link {{ request()->routeIs('roles.*') ? 'active' : '' }}" data-key="t-horizontal">Roles</a>
                                </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan

                @canany(['file.view', 'file.add', 'file.dmc-view', 'file.forward', 'file.inward', 'file.close'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('file.*') ? 'active' : '' }} {{ request()->routeIs('dmcfile.*') ? 'active' : '' }}" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="bx bx-user-circle"></i>
                        <span data-key="t-layouts">File Management</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('file.view')
                            <li class="nav-item">
                                <a href="{{ route('file.index') }}" class="nav-link {{ request()->routeIs('file.index') ? 'active' : '' }}" data-key="t-horizontal">Create File List</a>
                            </li>
                            @endcan

                            @can('file.forward')
                            <li class="nav-item">
                                <a href="{{ route('file.forward') }}" class="nav-link {{ request()->routeIs('file.forward') ? 'active' : '' }}" data-key="t-horizontal">Forward</a>
                            </li>
                            @endcan
                            @can('file.inward')
                            <li class="nav-item">
                                <a href="{{ route('file.inward') }}" class="nav-link {{ request()->routeIs('file.inward') ? 'active' : '' }}" data-key="t-horizontal">Inward</a>
                            </li>
                            @endcan

                            @can('file.transit')
                            <li class="nav-item">
                                <a href="{{ route('files.transit') }}" class="nav-link {{ request()->routeIs('files.transit') ? 'active' : '' }}" data-key="t-horizontal">Transit</a>
                            </li>
                            @endcan
                            @can('file.close')
                            <li class="nav-item">
                                <a href="{{ route('file.close') }}" class="nav-link {{ request()->routeIs('file.close') ? 'active' : '' }}" data-key="t-horizontal">Close</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan



                @canany(['file.verify', 'file.verified', 'file.rejected'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('file.*') ? 'active' : '' }} {{ request()->routeIs('dmcfile.*') ? 'active' : '' }}" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="bx bx-user-circle"></i>
                        <span data-key="t-layouts">DMC File</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('file.verify')
                            <li class="nav-item">
                                <a href="{{ route('dmcfile.index') }}" class="nav-link {{ request()->routeIs('dmcfile.index') ? 'active' : '' }}" data-key="t-horizontal">Verify File</a>
                            </li>
                            @endcan
                            @can('file.verified')
                            <li class="nav-item">
                                <a href="{{ route('dmcfile.verified') }}" class="nav-link {{ request()->routeIs('dmcfile.verified') ? 'active' : '' }}" data-key="t-horizontal">Verified File</a>
                            </li>
                            @endcan
                            @can('file.rejected')
                            <li class="nav-item">
                                <a href="{{ route('dmcfile.rejected') }}" class="nav-link {{ request()->routeIs('dmcfile.rejected') ? 'active' : '' }}" data-key="t-horizontal">Rejected File</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan


                @can('file.department-file')
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('department.getUserDepartmentFiles') ? 'active' : '' }}" href="{{ route('department.getUserDepartmentFiles') }}" >
                        <i class="ri-honour-line"></i>
                        <span data-key="t-dashboards">Department File</span>
                    </a>
                </li>
                @endcan

                @canany(['report.department', 'report.employees', 'report.file-status'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('report.*') ? 'active' : '' }}" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="bx bx-user-circle"></i>
                        <span data-key="t-layouts">Report</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('report.department')
                            <li class="nav-item">
                                <a href="{{ route('report.department') }}" class="nav-link {{ request()->routeIs('report.department') ? 'active' : '' }}" data-key="t-horizontal">Department Wise Report</a>
                            </li>
                            @endcan
                            @can('report.employees')
                            <li class="nav-item">
                                <a href="{{ route('report.employee') }}" class="nav-link {{ request()->routeIs('report.empolyees') ? 'active' : '' }}" data-key="t-horizontal">Employee Wise Report</a>
                            </li>
                            @endcan
                            @can('report.file-status')
                            <li class="nav-item">
                                <a href="{{ route('report.fileStatus') }}" class="nav-link {{ request()->routeIs('report.fileStatus') ? 'active' : '' }}" data-key="t-horizontal">File Status Report</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan

            </ul>
        </div>
    </div>

    <div class="sidebar-background"></div>
</div>


<div class="vertical-overlay"></div>
