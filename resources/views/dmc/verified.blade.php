<x-admin.layout>
    <x-slot name="title">Verified List</x-slot>
    <x-slot name="heading">Verified List</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Department</th>
                                    <th>User</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $file?->department?->name }}</td>
                                        <td>{{ $file?->user?->name }}</td>
                                        <td>{{ $file->file_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                        <td>{{ $file?->fileType?->name }}</td>
                                        <td>{{ $file->subject }}</td>
                                        <td>
                                            @if($file->dmc_status == "1")
                                            <span class="badge bg-danger">Rejected</span>
                                            @elseif($file->dmc_status == "2")
                                            <span class="badge bg-success">Accepetd</span>
                                            @else
                                            <span class="badge bg-success">Pending</span>
                                            @endif

                                        </td>
                                        <td>{{ ($file->dmc_remark) ? $file->dmc_remark : '-' }}</td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-admin.layout>




