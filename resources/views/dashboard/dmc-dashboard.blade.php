<x-admin.layout>
    <x-slot name="title">Dashboard</x-slot>
    <x-slot name="heading">Dashboard</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}

    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <div class="d-flex flex-column h-100">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12 col-lg-6">
                        <div class="card card-animate bg-info">
                            <div class="card-body">
                                <a href="javascript:void(0)">
                                    <div class="text-center">
                                        <p class="fw-medium text-white mb-0">
                                            Total File
                                        </p>
                                        <h2 class="mt-2 ff-secondary fw-semibold">
                                            <span class="counter-value text-white" data-target="{{ $totalFile }}">{{ $totalFile }}</span>
                                        </h2>
                                    </div>

                                </a>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col-->

                    <div class="col-md-6 col-sm-6 col-12 col-lg-6">
                        <div class="card card-animate bg-secondary">
                            <div class="card-body">
                                <a href="{{ route('dmcfile.index') }}">
                                    <div class="text-center">
                                        <p class="fw-medium text-white mb-0">
                                            Total Verify File
                                        </p>
                                        <h2 class="mt-2 ff-secondary fw-semibold">
                                            <span class="counter-value text-white" data-target="{{ $totalVerifyFile }}">{{ $totalVerifyFile }}</span>
                                        </h2>
                                    </div>

                                </a>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col-->

                    <div class="col-md-6 col-sm-6 col-12 col-lg-6">
                        <div class="card card-animate bg-success">
                            <div class="card-body">
                                <a href="{{ route('dmcfile.verified') }}">
                                    <div class="text-center">
                                        <p class="fw-medium text-white mb-0">
                                            Total Verified File
                                        </p>
                                        <h2 class="mt-2 ff-secondary fw-semibold">
                                            <span class="counter-value text-white" data-target="{{ $totalVerifiedFile }}">{{ $totalVerifiedFile }}</span>
                                        </h2>
                                    </div>

                                </a>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col-->

                    <div class="col-md-6 col-sm-6 col-12 col-lg-6">
                        <div class="card card-animate bg-danger">
                            <div class="card-body">
                                <a href="{{ route('dmcfile.rejected') }}">
                                    <div class="text-center">
                                        <p class="fw-medium text-white mb-0">
                                            Total Rejected File
                                        </p>
                                        <h2 class="mt-2 ff-secondary fw-semibold">
                                            <span class="counter-value text-white" data-target="{{ $totalRejectedFile }}">{{ $totalRejectedFile }}</span>
                                        </h2>
                                    </div>

                                </a>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col-->

                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-12">
            <div class="card border-primary card-height-100">

                <div class="card-header bg-primary align-items-center d-flex">
                    <h4 class="card-title text-white mb-0 flex-grow-1">Latest File List</h4>
                    <div class="flex-shrink-0">
                        <a href="{{ route('dmcfile.index') }}" class="btn btn-warning btn-sm">
                            View All
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1"
                            class="table table-bordered nowrap align-middle"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>File No.</th>
                                    <th>Date</th>
                                    <th>File Type</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($files as $file)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $file->file_no }}</td>
                                    <td>{{ date('d-m-Y', strtotime($file->date)) }}</td>
                                    <td>{{ $file?->fileType->name }}</td>
                                    <td>{{ $file->subject }}</td>
                                    <td>
                                        @if($file->dmc_status == "0")
                                        <span class="badge bg-warning text-dark">Pending</span>
                                        @elseif($file->dmc_status == "1")
                                        <span class="badge bg-danger text-dark">Rejected</span>
                                        @elseif($file->dmc_status == "2")
                                        <span class="badge bg-success text-dark">Accepted</span>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="7" align="center">No Data Found</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>



    @push('scripts')
    @endpush

</x-admin.layout>
