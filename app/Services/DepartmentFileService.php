<?php

namespace App\Services;

use App\Models\File;
use App\Models\UserDepartment;
use Illuminate\Support\Facades\Auth;

class DepartmentFileService
{
    public function getUserDepartmentFiles()
    {
        $userDepartments = UserDepartment::where("user_id", Auth::id())->pluck('department_id')->toArray();

        return File::with(['fileType'])->whereIn('department_id', $userDepartments)->latest()->get();
    }
}
