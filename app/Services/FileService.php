<?php

namespace App\Services;

use App\Models\File;
use App\Models\FileForwardInward;
use App\Models\FileType;
use App\Models\Department;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;
use App\Models\User;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class FileService
{
    // get user created file
    public function index()
    {
        return File::with(['fileType'])->where('user_id', Auth::user()->id)->where('is_file_forward', 0)->latest()->get();
    }

    public function transit()
    {
        return File::with(['fileType', 'fileForwardInward.to'])->where('is_file_forward', 1)->where('is_close', 0)->where('user_id', Auth::user()->id)->latest()->get();
    }

    // get file inward forward details
    public function showDetails($request)
    {
        return File::with(['user', 'department', 'fileForwardInwards' => function ($q) {
            return $q->with(['from.roles', 'to.roles', 'method', 'fromDepartment', 'toDepartment']);
        }, 'fileType'])->where('id', $request->id)->first();
    }

    // function to get qrcode data
    public function qrCode($request)
    {
        $file = File::with(['fileType'])->find($request->id);
        $html = "";

        if ($file) {
            $data = url('/file') . '\n' . 'पनवेल महानगरपालिका';
            $html .= '<div class="row">
                <div class="col-sm-12 col-lg-6 col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>File No.</th>
                                <td>' . $file->file_no . '</td>
                            </tr>
                            <tr>
                                <th>Date</th>
                                <td>' . date('d-m-Y', strtotime($file->date)) . '</td>
                            </tr>
                            <tr>
                                <th>File Type</th>
                                <td>' . $file?->fileType?->name . '</td>
                            </tr>
                            <tr>
                                <th>Subject</th>
                                <td>' . $file->subject . '</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 col-12">
                ' . QrCode::encoding('UTF-8')->size(170)->generate($data) . '</div>
            </div>';
        }

        return $html;
    }

    // store user created file details
    public function store($request)
    {
        DB::beginTransaction();
        try {
            if ($request->date != date("Y-m-d")) {
                $request['is_dmc'] = 1;
                $request['dmc_status'] = "0";
            }

            $department = Department::find($request->department_id);

            //generate file No
            $sequenceNo = ($department->sequence > 9) ? $department->sequence : '0' . $department->sequence;
            $fileNo = $department->initial . date('y') . $sequenceNo;
            $department->sequence = $department->sequence + 1;
            $department->save();

            $request['file_no'] = $fileNo;
            $request['to_id'] = Auth::user()->id;
            $request['is_forward'] = 1;
            $file = File::create($request->all());

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            return false;
        }
    }

    // get forward file list
    public function forward()
    {
        return File::with(['fileForwardInward'])->where([
            'to_id' => Auth::user()->id,
            'is_forward' => 1,
            'is_close' => 0
        ])->where(function ($q) {
            $q->whereNull('dmc_status')->orWhere('dmc_status', "2");
        })->latest()->get();
    }

    // function to get forward department by user role
    public function getForwardDepartment($request)
    {
        return Department::whereHas('userDepartments', function($q) use($request){
            $q->where('role_id', $request->role_id);
        })->get();
        // return UserDepartment::with(['department'])->where('role_id', $request->role_id)->get();
    }

    //function to get forward department user
    public function getForwardDepartmentUser($request)
    {
        return User::whereHas('userDepartments', function ($q) use ($request) {
            return $q->where([
                'department_id' => $request->department_id,
                'role_id' => $request->role_id,
            ])->where('user_id', '!=', Auth::user()->id);
        })->where('id', '!=', Auth::user()->id)->get();
    }

    // function to get forward administrative user
    public function getForwardAdministrativeUser($request)
    {
        return User::whereHas('userDepartments', function ($q) use ($request) {
            return $q->where('role_id', $request->role_id);
        })->where('id', '!=', Auth::user()->id)->get();
    }

    // get forward file details
    public function showForwardDetails($request)
    {
        return File::with(['user', 'department', 'fileForwardInward' => function ($q) {
            return $q->with(['from.roles', 'to.roles', 'method', 'fromDepartment', 'toDepartment']);
        }, 'fileType'])->where('id', '=', $request->id)->first();
    }

    // store forward user details
    public function saveForwardStatus($request)
    {
        DB::beginTransaction();
        try {
            $request['forward_date'] = date('Y-m-d h:i:s');
            $request['is_accepted'] = "0";



            if($request->file_inward_forward_id == 0){
                $file = File::find($request->file_id);
                $request['from_department_id'] = $file->department_id;
            }else{
                $departmentId = FileForwardInward::where('id', $request->file_inward_forward_id)->value('to_department_id');
                $request['from_department_id'] = $departmentId;
            }

            if ($request->department_id && $request->department_id != "") {
                $request['to_id'] = $request->department_user_id;
                $request['to_department_id'] = $request->department_id;
            } else {
                $request['to_id'] = $request->administrative_user_id;
            }
            FileForwardInward::create($request->all());

            // update file inward status
            File::where('id', $request->file_id)->update([
                'to_id' => $request->to_id,
                'is_forward' => 0,
                'is_file_forward' => 1
            ]);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            return false;
        }
    }


    // get inward file details
    public function inward($request)
    {
        return File::with(['fileForwardInward'])->where([
            'to_id' => Auth::user()->id,
            'is_forward' => 0,
            'is_close' => 0
        ])->where(function ($q) {
            $q->whereNull('dmc_status')->orWhere('dmc_status', "2");
        })->latest()->get();
    }

    // get forward file details
    public function showInwardDetails($request)
    {
        return File::with(['user', 'department', 'fileForwardInward' => function ($q) {
            return $q->with(['from.roles', 'to.roles', 'method', 'fromDepartment', 'toDepartment']);
        }, 'fileType'])->where('id', $request->id)->first();
    }

    // save inward details
    public function saveInwardStatus($request)
    {
        DB::beginTransaction();

        try {
            $file = FileForwardInward::find($request->file_forward_inward_id);
            $file->is_accepted = $request->is_accepted;
            $file->remark = $request->remark;
            $file->inward_date = date('Y-m-d h:i:s');
            $file->save();

            if ($request->is_accepted == "1") {
                $fileforward = new FileForwardInward;
                $fileforward->file_id = $file->file_id;
                $fileforward->from_department_id = $file->to_department_id;
                $fileforward->to_department_id = $file->from_department_id;
                $fileforward->from_id = $file->to_id;
                $fileforward->to_id = $file->from_id;
                $fileforward->method_id = $file->method_id;
                $fileforward->remark = $request->remark;
                $fileforward->forward_date = date('Y-m-d h:i:s');
                $fileforward->is_accepted = "0";
                $fileforward->is_rejected_back = 1;
                $fileforward->save();

                File::where('id', $request->file_id)->update([
                    'is_forward' => 0,
                    'to_id' => $file->from_id
                ]);

                DB::commit();
                return [true, "Inward file rejected successfully!"];
            } else {
                File::where('id', $request->file_id)->update([
                    'is_forward' => 1
                ]);
            }

            DB::commit();
            return [true, "Inward file accepted successfully!"];
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollback();
            return false;
        }
    }


    // function to get close file list
    public function close()
    {
        return File::with(['fileType'])->where([
            'user_id' => Auth::user()->id,
            'to_id' => Auth::user()->id,
            'is_file_forward' => 1
        ])->where(function($q){
            $q->whereNull('dmc_status')->orWhere('dmc_status', "2");
        })->latest()->get();
    }

    // function to close the file
    public function saveClose($request)
    {
        DB::beginTransaction();
        try {
            File::where('id', $request->file_id)->update([
                'close_remark' => $request->remark,
                'close_date' => date('Y-m-d h:i:s'),
                'is_close' => 1
            ]);

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollback();
            return false;
        }
    }
}
