<?php

namespace App\Services\Masters;

use App\Models\FileType;

class FileTypeService
{
    public function index()
    {
        return FileType::all();
    }

    public function store($request)
    {
        FileType::create($request->all());

        return true;
    }

    public function edit($id)
    {
        return FileType::find($id);
    }

    public function update($request, $id)
    {
        FileType::find($id)->update($request->all());

        return true;
    }
}
