<?php

namespace App\Services\Masters;

use App\Models\Department;

class DepartmentService
{
    public function index()
    {
        return Department::latest()->get();
    }

    public function store($request)
    {
        $request['sequence'] = 1;
        Department::create($request->all());

        return true;
    }

    public function edit($id)
    {
        return Department::find($id);
    }

    public function update($request, $id)
    {
        Department::find($id)->update($request->all());

        return true;
    }
}
