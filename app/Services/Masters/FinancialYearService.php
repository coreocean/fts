<?php

namespace App\Services\Masters;

use App\Models\FinancialYear;

class FinancialYearService
{
    public function index()
    {
        return FinancialYear::all();
    }

    public function store($request)
    {
        if ($request->status == 1) {
            FinancialYear::query()->update(['status' => 0]);
        }
        FinancialYear::create($request->all());

        return true;
    }

    public function edit($id)
    {
        return FinancialYear::find($id);
    }

    public function update($request, $id)
    {
        if ($request->status == 1) {
            FinancialYear::query()->update(['status' => 0]);
        }

        FinancialYear::find($id)->update($request->all());

        return true;
    }
}
