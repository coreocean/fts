<?php

namespace App\Services\Masters;

use App\Models\Method;

class MethodService
{
    public function index()
    {
        return Method::all();
    }

    public function store($request)
    {
        Method::create($request->all());

        return true;
    }

    public function edit($id)
    {
        return Method::find($id);
    }

    public function update($request, $id)
    {
        Method::find($id)->update($request->all());

        return true;
    }
}
