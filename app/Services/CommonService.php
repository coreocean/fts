<?php

namespace App\Services;

use App\Models\FileType;
use App\Models\FinancialYear;
use App\Models\User;
use App\Models\UserDepartment;
use App\Models\Department;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\Method;

class CommonService
{
    // function to get active method
    public function getActiveMethod()
    {
        return Method::whereStatus(1)->select('id', 'name')->get();
    }

    // function to get active user department
    public function getActiveUserDepartment()
    {
        return UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id')->toArray();
    }

    // function to get active file type
    public function getActiveFileType()
    {
        return FileType::whereStatus(1)->select('id', 'name')->get();
    }

    // function to get active financial year
    public function getActiveFinancialYear()
    {
        return FinancialYear::whereStatus(1)->select('id', 'year')->first();
    }

    // function to get current user details
    public function getCurrentUserLoginDetails()
    {
        return User::with(['userDepartments.department'])->find(Auth::user()->id);
    }

    // function to get the department with user not current user
    public function getAdministrativeAndDepartmentRole()
    {
        return Role::whereIn('name', ['User', 'Department HOD', 'Administrative AMC', 'Administrative DMC'])->get();
    }

    // function to get Active User
    public function getActiveUser(){
        return User::where('status', 1)->get();
    }

    // function to get active department
    public function getActiveDepartment($userDepartment){
        return Department::when(Auth::user()->roles[0]->name != "Super Admin", function($q) use($userDepartment){
            $q->whereIn('id', $userDepartment);
        })->whereStatus(1)->get();
    }
}
