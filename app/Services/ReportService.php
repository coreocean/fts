<?php

namespace App\Services;

use App\Models\Department;
use App\Models\UserDepartment;
use App\Models\File;
use Illuminate\Support\Facades\Auth;

class ReportService
{
    public function department()
    {
        $role = Auth::user()->roles[0]->name;
        $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id')->toArray();

        $departments = Department::query()
                     ->when($role != "Super Admin", function($q) use($userDepartments){
                        $q->whereIn('id', $userDepartments);
                     })->withCount(['files as created_count' => function($query){
                        $query->where([
                            'is_file_forward' => 0,
                            'is_close' => 0
                        ])->where(function($q){
                            $q->where('dmc_status', 0)->orWhereNull('dmc_status')->orWhere('dmc_status', 2);
                        });
                    }])->withCount(['files as dmc_rejected' => function($query){
                        $query->where(function($q){
                            $q->where('dmc_status', 1);
                        });
                    }])->withCount(['files as pending_count' => function($query){
                        $query->where([
                            'is_file_forward' => 1,
                            'is_close' => 0
                        ])->where(function($q){
                            $q->where('dmc_status', 0)->orWhereNull('dmc_status')->orWhere('dmc_status', 2);
                        });
                    }])->withCount(['files as closed_count' => function($query){
                        $query->where([
                            'is_file_forward' => 1,
                            'is_close' => 1
                        ])->where(function($q){
                            $q->where('dmc_status', 0)->orWhereNull('dmc_status')->orWhere('dmc_status', 2);
                        });
                    }])->get();


        return $departments;
    }

    public function employeeReports($request, $role, $userDepartments)
    {
        return File::with(['department', 'fileType', 'user'])->when($role != "Super Admin", function($q) use($userDepartments){
                        $q->whereIn('department_id', $userDepartments);
                     })
                     ->when(isset($request->user) && $request->user != "", function($q) use($request){
                        $q->where('user_id', $request->user);
                     })
                     ->when(isset($request->from) && $request->from != "", function($q) use($request){
                        $q->whereDate('date', '>=', $request->from);
                     })
                     ->when(isset($request->to) && $request->to != "", function($q) use($request){
                        $q->whereDate('date', '<=', $request->to);
                     })
                     ->latest()
                     ->get();
    }

    // get file status details
    public function getFileStatusDetails($departments, $users, $userDepartment){
        $departments = Department::withCount(['files as totalCreated'])->withCount([
            'files as pending' => function($q){
                $q->where([
                    'is_file_forward' => 1,
                    'is_close' => 0
                ]);
            }
        ])->withCount([
            'files as close' => function($q){
                $q->where([
                    'is_file_forward' => 1,
                    'is_close' => 1
                ]);
            }
        ])->withCount([
            'files as remaining' => function($q){
                $q->where([
                    'is_file_forward' => 0,
                    'is_close' => 0
                ]);
            }
        ])->when(Auth::user()->roles[0]->name != "Super Admin", function($q) use($userDepartment){
            $q->whereIn('id', $userDepartment);
        })->get();

        $data = [];
        foreach($departments as $department){
            $data[$department->id] = [
                'departmentId' => $department->id,
                'name' => $department->name,
                'totalCreated' => $department->totalCreated,
                'pending' => $department->pending,
                'remaining' => $department->remaining,
                'close' => $department->close
            ];
            $userData = [];
            foreach($users as $user){
                array_push($userData,
                    File::where([
                        'department_id' => $department->id,
                        'is_close' => 0,
                        'is_file_forward' =>1
                    ])->whereHas('fileForwardInward', function($q) use($user){
                        return $q->where('to_id', $user->id);
                    })->count()
                );
            }
            array_push($data[$department->id], $userData);
        }

        return $data;
    }
}
