<?php

namespace App\Services;

use App\Models\File;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDepartment;

class DMCFilePermissionService
{
    public function index()
    {
        $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');

        return File::with(['user', 'department'])->where(function($q){
            $q->whereNotNull('dmc_status')->where('dmc_status', "0");
        })->whereNotNull('dmc_status')->whereIn('department_id', $userDepartments)->latest()->get();
    }

    public function verified()
    {
        $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');

        return File::with(['user', 'department'])->where('dmc_status', "2")->whereIn('department_id', $userDepartments)->latest()->get();
    }

    public function rejected()
    {
        $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id');

        return File::with(['user', 'department'])->where('dmc_status', "1")->whereIn('department_id', $userDepartments)->latest()->get();
    }

    public function saveStatus($request)
    {
        $file = File::find($request->file_id);
        $file->dmc_status = $request->status;
        $file->dmc_user_id = Auth::user()->id;
        $file->dmc_status_date = date('Y-m-d');
        $file->dmc_remark = $request->remark;
        $file->save();
        return true;
    }
}
