<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileForwardInward extends Model
{
    use HasFactory;

    protected $fillable = ['file_id', 'from_department_id', 'from_id', 'to_department_id', 'to_id', 'method_id', 'is_accepted', 'remark', 'person_name', 'forward_date', 'inward_date'];

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }

    public function fromDepartment()
    {
        return $this->belongsTo(Department::class, 'from_department_id', 'id');
    }

    public function toDepartment()
    {
        return $this->belongsTo(Department::class, 'to_department_id', 'id');
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'from_id', 'id');
    }

    public function to()
    {
        return $this->belongsTo(User::class, 'to_id', 'id');
    }

    public function method()
    {
        return $this->belongsTo(Method::class, 'method_id', 'id');
    }
}
