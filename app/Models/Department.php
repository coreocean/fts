<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\File;
use App\Models\UserDepartment;

class Department extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'status', 'sequence', 'initial'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function files(){
        return $this->hasMany(File::class, 'department_id', 'id');
    }

    public function userDepartments(){
        return $this->hasMany(UserDepartment::class, 'department_id', 'id');
    }
}
