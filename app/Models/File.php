<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = ['file_type_id', 'user_id', 'financial_year_id', 'file_no', 'is_dmc', 'dmc_user_id', 'dmc_status', 'dmc_status_date', 'dmc_remark', 'subject', 'date', 'is_close', 'close_date', 'to_id', 'is_forward', 'department_id', 'is_file_forward'];

    public function fileType()
    {
        return $this->belongsTo(FileType::class, 'file_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function dmcUser()
    {
        return $this->belongsTo(User::class, 'dmc_user_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function fileForwardInwards()
    {
        return $this->hasMany(FileForwardInward::class, 'file_id', 'id');
    }

    public function fileForwardInward()
    {
        return $this->hasOne(FileForwardInward::class, 'file_id', 'id')->latestOfMany();
    }
}
