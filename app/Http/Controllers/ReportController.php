<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ReportService;
use App\Services\CommonService;
use App\Models\User;
use App\Models\UserDepartment;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    protected $reportService;
    protected $commonService;

    public function __construct(ReportService $reportService, CommonService $commonService)
    {
        $this->reportService = $reportService;
        $this->commonService = $commonService;
    }

    public function department()
    {
        $departments = $this->reportService->department();

        return view('report.department')->with([
            'departments' => $departments
        ]);
    }

    public function employee(Request $request)
    {
        $role = Auth::user()->roles[0]->name;
        $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id')->toArray();

        $reports = $this->reportService->employeeReports($request, $role, $userDepartments);

        $employees = User::when($role != "Super Admin", function($q) use($userDepartments){
            $q->whereHas('userDepartments', function($q) use($userDepartments){
                $q->whereIn('department_id', $userDepartments);
            });
        })->get();

        return view('report.employee')->with([
            'reports' => $reports,
            'employees' => $employees
        ]);
    }

    // function to get file status details
    public function fileStatus(Request $request){
        $userDepartment = $this->commonService->getActiveUserDepartment();

        $departments = $this->commonService->getActiveDepartment($userDepartment);

        $users = $this->commonService->getActiveUser();

        $fileStatus = $this->reportService->getFileStatusDetails($departments, $users, $userDepartment);
        // return $fileStatus;
        return view('report.file_status')->with([
            'fileStatus' => $fileStatus,
            'departments' => $departments,
            'users' => $users
        ]);
    }
}
