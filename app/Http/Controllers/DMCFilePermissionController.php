<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DMCFilePermissionService;
use App\Http\Requests\DMCFileRequest;

class DMCFilePermissionController extends Controller
{
    protected $dmcFilePermissionService;

    public function __construct(DMCFilePermissionService $dmcFilePermissionService)
    {
        $this->dmcFilePermissionService = $dmcFilePermissionService;
    }

    public function index()
    {
        $files = $this->dmcFilePermissionService->index();

        return view('dmc.verify')->with([
            'files' => $files
        ]);
    }

    public function store(DMCFileRequest $request)
    {
        if ($request->ajax()) {
            $file = $this->dmcFilePermissionService->saveStatus($request);

            if ($file) {
                return response()->json(['success' => 'File status updated successfully']);
            } else {
                return response()->json(['error' => 'Something went wrong!']);
            }
        }
    }

    public function verified()
    {
        $files = $this->dmcFilePermissionService->verified();

        return view('dmc.verified')->with([
            'files' => $files
        ]);
    }

    public function rejected()
    {
        $files = $this->dmcFilePermissionService->rejected();

        return view('dmc.rejected')->with([
            'files' => $files
        ]);
    }
}
