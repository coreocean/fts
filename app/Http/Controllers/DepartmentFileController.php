<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DepartmentFileService;

class DepartmentFileController extends Controller
{
    protected $departmentFileService;

    public function __construct(DepartmentFileService $departmentFileService)
    {
        $this->departmentFileService = $departmentFileService;
    }

    public function getUserDepartmentFiles()
    {
        $files = $this->departmentFileService->getUserDepartmentFiles();

        return view("department-file.index")->with([
            'files' => $files
        ]);
    }
}
