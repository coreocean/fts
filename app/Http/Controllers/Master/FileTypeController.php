<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Master\FileTypeRequest;
use App\Services\Masters\FileTypeService;

class FileTypeController extends Controller
{
    protected $fileTypeService;

    public function __construct(FileTypeService $fileTypeService)
    {
        $this->fileTypeService = $fileTypeService;
    }

    public function index()
    {
        $fileTypes = $this->fileTypeService->index();

        return view('master.file-type.index')->with(['fileTypes' => $fileTypes]);
    }

    public function store(FileTypeRequest $request)
    {
        $fileType = $this->fileTypeService->store($request);

        if ($fileType) {
            return response()->json(['success' => 'File type created successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    public function edit(Request $request, $id)
    {
        $fileType = $this->fileTypeService->edit($id);

        if ($fileType) {
            $response = [
                'result' => 1,
                'fileType' => $fileType,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function update(FileTypeRequest $request, $id)
    {
        $fileType = $this->fileTypeService->update($request, $id);

        if ($fileType) {
            return response()->json(['success' => 'File type updated successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }
}
