<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Master\DepartmentRequest;
use App\Services\Masters\DepartmentService;

class DepartmentController extends Controller
{
    protected $departmentService;

    public function __construct(DepartmentService $departmentService)
    {
        $this->departmentService = $departmentService;
    }

    public function index()
    {
        $departments = $this->departmentService->index();

        return view('master.department.index')->with(['departments' => $departments]);
    }

    public function store(DepartmentRequest $request)
    {
        $department = $this->departmentService->store($request);

        if ($department) {
            return response()->json(['success' => 'Department created successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    public function edit(Request $request, $id)
    {
        $department = $this->departmentService->edit($id);

        if ($department) {
            $response = [
                'result' => 1,
                'department' => $department,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DepartmentRequest $request, $id)
    {
        $department = $this->departmentService->update($request, $id);

        if ($department) {
            return response()->json(['success' => 'Department updated successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }
}
