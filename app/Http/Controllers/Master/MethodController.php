<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Master\MethodRequest;
use App\Services\Masters\MethodService;

class MethodController extends Controller
{
    protected $methodService;

    public function __construct(MethodService $methodService)
    {
        $this->methodService = $methodService;
    }

    public function index()
    {
        $methods = $this->methodService->index();

        return view('master.method.index')->with(['methods' => $methods]);
    }

    public function store(MethodRequest $request)
    {
        $method = $this->methodService->store($request);

        if ($method) {
            return response()->json(['success' => 'Method created successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    public function edit(Request $request, $id)
    {
        $method = $this->methodService->edit($id);

        if ($method) {
            $response = [
                'result' => 1,
                'method' => $method,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function update(MethodRequest $request, $id)
    {
        $method = $this->methodService->update($request, $id);

        if ($method) {
            return response()->json(['success' => 'Method updated successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }
}
