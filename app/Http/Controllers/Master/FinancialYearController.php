<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Master\FinancialYearRequest;
use App\Services\Masters\FinancialYearService;
use App\Models\FinancialYear;

class FinancialYearController extends Controller
{
    protected $financialYearService;

    public function __construct(FinancialYearService $financialYearService)
    {
        $this->financialYearService = $financialYearService;
    }

    public function index()
    {
        $financialYears = $this->financialYearService->index();

        return view('master.financial-year.index')->with(['financialYears' => $financialYears]);
    }

    public function store(FinancialYearRequest $request)
    {
        $fromDateCheck = FinancialYear::whereDate('from_date', '<=', date('Y-m-d', strtotime($request->from_date)))
            ->whereDate('to_date', '>=', date('Y-m-d', strtotime($request->from_date)))
            ->exists();

        $toDateCheck = FinancialYear::whereDate('from_date', '<=', date('Y-m-d', strtotime($request->to_date)))
            ->whereDate('to_date', '>=', date('Y-m-d', strtotime($request->to_date)))
            ->exists();
        if ($fromDateCheck || $toDateCheck) {
            return response()->json(['error' => 'Financial Year Already Exists!']);
        }

        $financialYear = $this->financialYearService->store($request);

        if ($financialYear) {
            return response()->json(['success' => 'Financial year created successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    public function edit(Request $request, $id)
    {
        $financialYear = $this->financialYearService->edit($id);

        if ($financialYear) {
            $response = [
                'result' => 1,
                'financialYear' => $financialYear,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function update(FinancialYearRequest $request, $id)
    {
        $fromDateCheck = FinancialYear::where('from_date', '<=', date('Y-m-d', strtotime($request->from_date)))
                ->where('to_date', '>=', date('Y-m-d', strtotime($request->from_date)))
                ->where('id', '!=', $request->edit_model_id)
                ->exists();

        $toDateCheck = FinancialYear::where('from_date', '<=', date('Y-m-d', strtotime($request->to_date)))
            ->where('to_date', '>=', date('Y-m-d', strtotime($request->to_date)))
            ->where('id', '!=', $request->edit_model_id)
            ->exists();

        if ($fromDateCheck || $toDateCheck) {
            return response()->json(['error' => 'Financial Year Already Exists!']);
        }

        $financialYear = $this->financialYearService->update($request, $id);

        if ($financialYear) {
            return response()->json(['success' => 'Financial year updated successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }
}
