<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FileService;
use App\Http\Requests\FileRequest;
use App\Services\CommonService;
use App\Http\Requests\InwardFileRequest;
use App\Http\Requests\ForwardFileRequest;


class FileController extends Controller
{
    protected $fileService;
    protected $commonService;

    public function __construct(FileService $fileService, CommonService $commonService)
    {
        $this->fileService = $fileService;
        $this->commonService = $commonService;
    }

    // get file details
    public function index()
    {
        $files = $this->fileService->index();

        $fileTypes = $this->commonService->getActiveFileType();

        $financialYear = $this->commonService->getActiveFinancialYear();

        $user = $this->commonService->getCurrentUserLoginDetails();
       
        return view("file.index")->with([
            'files' => $files,
            'fileTypes' => $fileTypes,
            'financialYear' => $financialYear,
            'user' => $user,
        ]);
    }

    // function to get qrcode details
    public function qrCode(Request $request)
    {
        if ($request->ajax()) {

            $data = $this->fileService->qrCode($request);

            return response()->json([
                'data' => $data
            ]);
        }
    }

    // get All details
    public function showDetails(Request $request)
    {
        if ($request->ajax()) {
            $file = $this->fileService->showDetails($request);

            if ($file) {
                return response()->json(['file' => $file]);
            } else {
                return response()->json(['error' => 'Something went wrong!']);
            }
        }
    }

    // store file details
    public function store(FileRequest $request)
    {
        $file = $this->fileService->store($request);

        if ($file) {
            return response()->json(['success' => 'File created successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    // get forward file
    public function forward(Request $request)
    {
        $files = $this->fileService->forward();

        $roles = $this->commonService->getAdministrativeAndDepartmentRole();

        $methods = $this->commonService->getActiveMethod();

        return view("file.forward")->with([
            'files' => $files,
            'roles' => $roles,
            'methods' => $methods
        ]);
    }

    // function to get ajax forward department
    public function getForwardDepartment(Request $request)
    {
        if ($request->ajax()) {
            $departments = $this->fileService->getForwardDepartment($request);

            return response()->json(['departments' => $departments]);
        }
    }

    // function to get department users
    public function getForwardDepartmentUser(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $users = $this->fileService->getForwardDepartmentUser($request);

            return response()->json(['users' => $users]);
        }
    }

    // funtion to get all administrative user
    public function getForwardAdministrativeUser(Request $request)
    {
        if ($request->ajax()) {
            $users = $this->fileService->getForwardAdministrativeUser($request);

            return response()->json(['users' => $users]);
        }
    }

    // get forward details
    public function showForwardDetails(Request $request)
    {
        $file = $this->fileService->showForwardDetails($request);

        if ($file) {
            return response()->json(['file' => $file]);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    // save forward file
    public function storeForward(ForwardFileRequest $request)
    {
        // dd($request);
        $file = $this->fileService->saveForwardStatus($request);

        if ($file) {
            return response()->json(['success' => 'File forwarded successfully!']);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    // get inward file
    public function inward(Request $request)
    {
        $files = $this->fileService->inward($request);
        // return auth()->user()->id;
        // return $files;
        return view("file.inward")->with([
            'files' => $files,
        ]);
    }

    // get forward details
    public function showInwardDetails(Request $request)
    {
        $file = $this->fileService->showInwardDetails($request);

        if ($file) {
            return response()->json(['file' => $file]);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    // save inward file status
    public function storeInward(InwardFileRequest $request)
    {
        $file = $this->fileService->saveInwardStatus($request);

        if ($file[0]) {
            return response()->json(['success' => $file[1]]);
        } else {
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    // function to get transit record
    public function transit(Request $request)
    {
        $files = $this->fileService->transit();
        
        return view('file.transit')->with([
            'files' => $files
        ]);
    }

    // function to get close file list
    public function close(Request $request)
    {
        $files = $this->fileService->close($request);
       
        return view("file.close")->with([
            'files' => $files,
        ]);
    }

    // save close file data
    public function saveClose(Request $request)
    {
        if ($request->ajax()) {
            $file = $this->fileService->saveClose($request);

            if ($file) {
                return response()->json(['success' => 'File closed successfully!']);
            } else {
                return response()->json(['error' => 'Something went wrong!']);
            }
        }
    }
}
