<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use App\Models\File;
use App\Models\UserDepartment;

class DashboardController extends Controller
{

    public function index()
    {
        $role = Auth::user()->roles[0]->name;
        if($role == 'Administrative DMC'){
            $userDepartments = UserDepartment::where('user_id', Auth::user()->id)->pluck('department_id')->toArray();

            $totalFile = File::whereNotNull('dmc_status')->whereIn('department_id', $userDepartments)->count();

            $totalVerifyFile = File::where('dmc_status', "0")->whereIn('department_id', $userDepartments)->count();

            $totalVerifiedFile = File::where('dmc_status', "2")->whereIn('department_id', $userDepartments)->count();

            $totalRejectedFile = File::where('dmc_status', "1")->whereIn('department_id', $userDepartments)->count();

            $files = File::with(['fileType'])->whereNotNull('dmc_status')->whereIn('department_id', $userDepartments)->latest()->limit(5)->get();

            return view('dashboard.dmc-dashboard')->with([
                'totalFile' => $totalFile,
                'totalVerifyFile' => $totalVerifyFile,
                'totalVerifiedFile' => $totalVerifiedFile,
                'totalRejectedFile' => $totalRejectedFile,
                'files' => $files
            ]);
        }else{
            $totalFile = File::where('user_id', Auth::user()->id)->count();

            $totalCreatedFile = File::where('user_id', Auth::user()->id)->where('is_file_forward', 0)->count();

            $totalCloseFile = File::where('user_id', Auth::user()->id)->where('is_close', 1)->count();

            $totalForwardFile = File::where([
                'user_id' => Auth::user()->id,
                'is_close' => 0,
                'is_file_forward' => 1
                ])->count();

            $totalAcceptedFileByDmc = File::where(['user_id' => Auth::user()->id, 'dmc_status' => "2"])->count();

            $totalRejectedFileByDmc = File::where(['user_id' => Auth::user()->id, 'dmc_status' => "1"])->count();

            $files = File::with(['fileType'])->where('user_id', Auth::user()->id)->latest()->limit(5)->get();

            return view('dashboard.dashboard')->with([
                'totalFile' => $totalFile,
                'totalAcceptedFileByDmc' => $totalAcceptedFileByDmc,
                'totalRejectedFileByDmc' => $totalRejectedFileByDmc,
                'files' => $files,
                'totalCloseFile' => $totalCloseFile,
                'totalForwardFile' => $totalForwardFile,
                'totalCreatedFile' => $totalCreatedFile
            ]);
        }

    }

    public function changeThemeMode()
    {
        $mode = request()->cookie('theme-mode');

        if ($mode == 'dark')
            Cookie::queue('theme-mode', 'light', 43800);
        else
            Cookie::queue('theme-mode', 'dark', 43800);

        return true;
    }
}
