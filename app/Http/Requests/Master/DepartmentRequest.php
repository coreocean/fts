<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        if ($this->edit_model_id) {
            $rule = [
                'name' => "required|unique:departments,name,$this->edit_model_id",
                'status' => 'required',
                'initial' => "required|unique:departments,initial,$this->edit_model_id"
            ];
        } else {
            $rule = [
                'name' => 'required|unique:departments,name',
                'status' => 'required',
                'initial' => 'required|unique:departments,initial'
            ];
        }

        return $rule;
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Please enter name',
            'name.unique' => 'Department Already exists',
            'status.required' => 'Please select status',
            'initial.required' => 'Please enter initial',
            'initial.unique' => 'Department Already exists',
        ];
    }
}
