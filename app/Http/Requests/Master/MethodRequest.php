<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;

class MethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        if ($this->edit_model_id) {
            $rule = [
                'name' => "required|unique:methods,name,$this->edit_model_id",
                'status' => 'required'
            ];
        } else {
            $rule = [
                'name' => 'required|unique:methods,name',
                'name.unique' => 'Department Already exists',
                'status' => 'required'
            ];
        }

        return $rule;
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Please enter name',
            'status.required' => 'Please select status'
        ];
    }
}
