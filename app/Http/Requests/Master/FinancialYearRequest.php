<?php

namespace App\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;

class FinancialYearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'status' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'year.required' => 'Please enter year',
            'from_date.required' => 'Please select from date',
            'to_date.required' => 'Please select to date',
            'status.required' => 'Please select status'
        ];
    }
}
