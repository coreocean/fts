<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'date' => 'required',
            'file_type_id' => 'required',
            'subject' => 'required',
            'user_id' => 'required',
            'financial_year_id' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'date.required' => 'Please select date',
            'file_type_id.required' => 'Please select file type',
            'subject.required' => 'Please enter subject',
            'user_id' => 'User is required',
            'financial_year_id' => 'Please select financial year'
        ];
    }
}
